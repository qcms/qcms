<?php

namespace App\Http\Livewire\Admin;

use App\Models\User;
use Livewire\Component;
use Qcms\Qdatatable\Facades\Qdatatable;

class UserManager extends Component
{
    public function render()
    {
        $datatable = Qdatatable::datatable(User::class);
        $datatable->render();
        return view('livewire.admin.user-manager', [$datatable]);
    }
}
